//
//  Articles.swift
//  JSONTest
//
//  Created by Apple on 16/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper



//Article Model
class  Articles:Mappable {
    var id:Int = 0
    var title: String?
    var desc: String?
    var createdDate: String?
    var status: String?
    var image: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        desc <- map["DESCRIPTION"]
        createdDate <- map["CREATED_DATE"]
        status <- map["STATUS"]
        image <- map["IMAGE"]
    }
}



