//
//  ViewController.swift
//  JSONTest
//
//  Created by Apple on 14/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController {
    var articles =  [Articles]()
    var totalArticle:Int = 0
    static var limit = 10 , page = 1
    var refresh : UIRefreshControl!
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cell = UINib(nibName: "ArticleTableViewCell", bundle: nil)
        self.table.register(cell, forCellReuseIdentifier: "tableCell")
        
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Load More Article")
        refresh.addTarget(self, action: #selector(refreshArticleData), for: UIControl.Event.valueChanged)
        table.addSubview(refresh)
        
        loadData(limit: ViewController.limit,page: ViewController.page)
        
        
        
        

    }
    
    //load data
    func loadData(limit: Int, page: Int) {
        let urlString = "http://api-ams.me/v1/api/articles?page=\(page)&limit=\(limit)"
        let header = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers:header ).responseJSON{
            (response) in
            DispatchQueue.main.async {
                if let obj = response.result.value as! [String: Any]?{
                    let page = obj["PAGINATION"] as! [String: Int]
                    self.totalArticle = page["TOTAL_COUNT"]!
                    
                    let articleObj = obj["DATA"] as! [[String:Any]]
                    for d in articleObj{
                        self.articles.append(Articles(JSON: d)!)
                        
                    }
                    self.table.reloadData()
                }
                else{
                    print("No Data")
                }
                
            }
            
            
        }
    }
    
    //refresh data
    @objc func refreshArticleData() {
        articles = []
        loadData(limit: 10, page: 1)
        refresh.endRefreshing()
    }

}
extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! ArticleTableViewCell
            cell.setCellInfo(article: articles[indexPath.row])
        return cell
    }
    
    //when click on cell open detail form
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let detailForm = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        self.navigationController?.pushViewController(detailForm, animated: true)
        
//        set article data
//        detailForm.articleTitle.text = articles[indexPath.row].title!
//        detailForm.articleDescription.text = articles[indexPath.row].desc!
//        detailForm.articleCreatedDate.text = articles[indexPath.row].createdDate!
//        let url = URL(string: articles[indexPath.row].image!)
//        detailForm.articleImage.kf.setImage(with: url, placeholder: UIImage(named: "DefaultImage"))
        
        
        
    }
}

extension ViewController: UITableViewDelegate{
    //disply on scroll
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if indexPath.row == self.articles.count-1{
                ViewController.limit += 10
                ViewController.page += 1
                self.loadData(limit: ViewController.limit, page: ViewController.page)
            }
            
        }
        
    }
    
    
    //delete article
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete"){
            (action, view, completion) in
            self.deleteArticleFromDB(at: self.articles[indexPath.row].id)
            self.articles.remove(at: indexPath.row)
            self.table.deleteRows(at: [indexPath], with: .automatic)
            completion(true)
        }
        action.image = UIImage(named: "Delete")
        action.backgroundColor = .red
        return action
    }
    
    func deleteArticleFromDB(at indexPath: Int) {
        let urlString = "http://api-ams.me/v1/api/articles/\(indexPath)"
        let header = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        Alamofire.request(urlString, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers:header ).responseJSON{
            (response) in
             if let result = response.result.value as! [String: Any]?{
                print(result["MESSAGE"] ?? "Delete no success")
            }
           
        }
    }
    
    //edit article
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [edit])
    }
    
    func editAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Edit"){
            (action, view, completion) in
            
            guard  let updateViw = self.storyboard?.instantiateViewController(withIdentifier: "UpdateViewController") as? UpdateViewController else {
                fatalError("View  not found")
            }
//            updateViw.delegate = self
            self.navigationController?.pushViewController(updateViw, animated: true)
            
//            self.articles.remove(at: indexPath.row)
//            self.table.deleteRows(at: [indexPath], with: .automatic)
//            completion(true)
            
        }
        action.image = UIImage(named: "Edit")
        action.backgroundColor = .green
        return action
    }//end editing article
}
