//
//  ArticleTableViewCell.swift
//  JSONTest
//
//  Created by Apple on 17/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var creadtedDate: UILabel!
    @IBOutlet weak var imageContainer: UIImageView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    func setCellInfo(article: Articles)  {
        self.desc.text = article.desc
        
        //set title style
        self.title.text = article.title
        title.adjustsFontForContentSizeCategory = true
        self.title.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        
        
            //set image
            let url = URL(string: article.image!)
            self.imageContainer.kf.setImage(with: url, placeholder: UIImage(named: "DefaultImage"))
        
        
        //date formate
        let timeInterval  = 1415639000.67457
        let date = NSDate(timeIntervalSince1970: timeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.date(from: article.createdDate!)
        dateFormatter.dateFormat = "MMM-dd h:mm a"
        let mydate = dateFormatter.string(from: date as Date)
        
        self.creadtedDate.text = String(mydate) + String(article.id)
    }
    
}




